from rest_framework import serializers
from .models import User, CreatedTrip

class UserSerializer(serializers.Serializer):
    username = serializers.CharField()
    first_name = serializers.CharField(max_length=120)
    last_name = serializers.CharField(max_length=120)
    email = serializers.CharField()
    contact = serializers.CharField()
    user_role = serializers.CharField()
    created_at = serializers.DateTimeField()

    # author_id = serializers.IntegerField()

    def create(self, validated_data):
        return User.objects.create(**validated_data)
    
    def update(self, instance, validated_data):
        instance.first_name = validated_data.get('name', instance.first_name)
        instance.email = validated_data.get('email', instance.email)
        instance.contact = validated_data.get('contact', instance.contact)

        instance.save()
        return instance

class CreatedTripSerializer(serializers.Serializer):
    owner = serializers.CharField
    destination = serializers.CharField(max_length=120)
    date = serializers.DateTimeField()
    number_of_joiner = serializers.IntegerField()
    number_of_day = serializers.IntegerField()
    price = serializers.DecimalField(max_digits=10, decimal_places=2, default=0)
    itinerary = serializers.CharField()

    def create(self, validated_data):
        return CreatedTrip.objects.create(**validated_data)

    def update(self, instace, validated_data):
        instance.destination = validated_data.get('destination', instance.destination)
        instance.date = validated_data.get('date', instance.date)
        instance.itinerary = validated_data.get('itinerary', instance.itinerary)

        instance.save()
        return instance