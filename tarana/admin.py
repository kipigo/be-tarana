from django.contrib import admin
from tarana.models import *
# Register your models here.

admin.site.register(CreatedTrip)
admin.site.register(SuggestedTrip)
@admin.register(User)

class OriginAdmin(admin.ModelAdmin):
    list_display = ("first_name", "created_at")