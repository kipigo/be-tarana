from django.shortcuts import render

# Create your views here.
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User, CreatedTrip
from .serializers import UserSerializer, CreatedTripSerializer

class UserView(APIView):
    def get(self, request):
        users = User.objects.all()
        serializer = UserSerializer(users, many=True)
        return Response({"users": serializer.data})

    def post(self, request):
        user = request.data.get('user')
        serializer = UserSerializer(data=user)

        if serializer.is_valid(raise_exception=True):
            user_saved = serializer.save()
        
        return Response({"success": "User '{}' created sucessfully".format(user_saved.first_name)})

    def put(self, request, pk):
        saved_user = get_object_or_404(User.objects.all(), pk=pk)
        data = request.data.get('user')
        serializer = UserSerializer(instance=saved_user, data=data, partial=True)
        
        if serializer.is_valid(raise_exception=True):
            user_saved = serializer.save()

        return Response({"success": "User '{}' updated sucessfully".format(user_saved.first_name)})

    def delete(self, request, pk):
        user = get_object_or_404(User.objects.all(), pk=pk)
        user.delete()

        return Response({"message": "User with id `{}` has been deleted".format(pk)}, status=204)


class CreatedTripView(APIView):
    def get(self, request):
        created = CreatedTrip.objects.all()
        serializer = CreatedTripSerializer(created, many=True)
        return Response({"created_trip": serializer.data})

    def post(self, request):
        create = request.data.get('create')
        serializer = CreatedTripSerializer(data=create)

        if serializer.is_valid(raise_exception=True):
            create_saved = serializer.save()
        
        return Response({"success": "Created Trip to '{}' sucessfully".format(create_saved.destination)})

    def put(self, request, pk):
        saved_created = get_object_or_404(CreatedTrip.objects.all(), pk=pk)
        data = request.data.get('create')
        serializer = CreatedTripSerializer(instasnce=saved_created, data=data, partial=True)

        if serializer.is_valid(raise_exception=True):
            create_saved = serializer.save()

        return Response({"success": "Updated trip to '{}' sucessfully".format(create_saved.destination)})
        
        