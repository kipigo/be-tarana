from django.urls import path
from .views import UserView, CreatedTripView


app_name = 'users'


urlpatterns = [
    #user urls
    path('user/', UserView.as_view()),
    path('user/<int:pk>', UserView.as_view()),
    path('created/', CreatedTripView.as_view()),
    path('created/<int:pk>', CreatedTripView.as_view()),

]

