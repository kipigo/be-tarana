# Generated by Django 2.2.4 on 2019-08-15 08:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tarana', '0002_createdtrips'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='CreatedTrips',
            new_name='CreatedTrip',
        ),
    ]
