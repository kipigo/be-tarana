from django.db import models


class User(models.Model):
    SUPERADMIN = 'AD'
    USER = 'US'
    SUPPLIER = 'SU'

    USER_ROLES= (
        (SUPERADMIN, 'SuperAdmin'), (USER,'User'), (SUPPLIER,'Supplier')
    )

    USER_ROLE_CHOICE = {
        SUPERADMIN: 'SuperAdmin',
        USER: 'User',
        SUPPLIER: 'Supplier'

    }
    
    username = models.CharField(max_length=40, unique=True)
    email = models.EmailField(null=True)
    first_name = models.CharField(max_length=40, blank=True)
    last_name = models.CharField(max_length=40, blank=True)
    contact = models.CharField(max_length=20, blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    user_role = models.CharField(
        max_length=5, choices=USER_ROLES, default='US')


class CreatedTrip(models.Model):

    owner = models.ForeignKey('User', related_name='trip',on_delete=models.CASCADE, null=True, blank=True)
    category = models.ForeignKey('SuggestedTrip', related_name='trip',on_delete=models.CASCADE, null=True, blank=True)
    destination = models.CharField(max_length=120)
    date = models.DateTimeField()
    number_of_joiner = models.IntegerField()
    number_of_day = models.IntegerField()
    maximum = models.CharField(max_length=40, default=15)
    status = models.CharField(max_length=120, default="Open")
    price = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    itinerary = models.TextField()

    def __str__(self):
        return self.destination


class SuggestedTrip(models.Model):
    title = models.CharField(max_length=120)
    description = models.CharField(max_length=200)

    def __str__(self):
        return self.title



